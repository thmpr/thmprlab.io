+++
title = "Thmpr"


# The homepage contents
[extra]
lead = 'Intelligent Machines Laboratory'
url = "/docs/getting-started/introduction/"
url_button = "Enter the IML"
repo_version = "GitHub v0.1.0"
repo_license = "Open-source cc-sa license."
repo_url = "https://gitlab.com/thmpr"

[[extra.list]]
title = "Correct by construction 💯️"
content = "software compiles <i>only if</i> it satisfies specification constraints"

[[extra.list]]
title = "Machine Intelligence"
content = "adaptive asynchronous code on distributed fault-tolerant networks"

[[extra.list]]
title = "Proof carrying code 🔒️"
content = "algorithms packaged with machine-verified proofs of correctness"

+++
