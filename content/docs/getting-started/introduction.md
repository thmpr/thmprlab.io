+++
title = "Introduction"
description = ""
date = 2021-05-01T08:00:00+00:00
updated = 2021-05-01T08:00:00+00:00
draft = false
weight = 10
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = 'Here we document the core Thmpr technologies and demonstrate how to use them for conjecture generation, decision making, formal proof, and software verification.'
toc = true
top = false
+++

## Quick Start

One page summary of how to install each of the core Thmpr technologies [Quick Start →](../quick-start/)

## Go further

Take a deeper dive into the documentation to get the most out of Thmpr technologies. [Deep Dive →](../deep-dive/)

## Contributing

Find out how to contribute to Doks. [Contributing →](../../contributing/how-to-contribute/)

## Help

Get help on Doks. [Help →](../../help/faq/)

## Credits

Find out who's responsible. [Credits →](../credits/)

