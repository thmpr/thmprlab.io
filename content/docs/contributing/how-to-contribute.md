+++
title = "How to Contribute"
description = "Contribute to agda-algebras, improve documentation, or submit code."
date = 2021-12-17T18:10:00+00:00
updated = 2021-12-17T18:10:00+00:00
draft = false
weight = 410
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Contribute to agda-algebras, improve documentation, or submit new code."
toc = true
top = false
+++

👉 Make sure to read the [Code of Conduct](../code-of-conduct/).

## Contribute to Doks

👉 The agda-algebras code lives in the [`ualib/agda-algebras` repository](https://github.com/ualib/agda-algebras)

- Follow the [GitHub flow](https://guides.github.com/introduction/flow/).
- Follow the [Conventional Commits Specification](https://www.conventionalcommits.org/en/v1.0.0/)

### Create an issue

- [Bug report](https://github.com/ualib/agda-algebras/issues/new?template=bug-report---.md)
- [Feature request](https://github.com/ualib/agda-algebras/issues/new?template=feature-request---.md)

## Improve documentation

👉 The `agda-algebras` documentation is generated from literate Agda source code files which lives in the [`./src`](https://github.com/ualib/agda-algebras/tree/master/src) subdirectory
of the [`agda-algebras` repository](https://github.com/ualib/agda-algebras).

- Follow the [GitHub flow](https://guides.github.com/introduction/flow/).
- Follow the [Conventional Commits Specification](https://www.conventionalcommits.org/en/v1.0.0/)
